# Demos

### This repository will act as the holding area for all Demos and will act as my Portfolio. It is not intended to contain any actual code, thus all uploaded files should be able to be downloaded and used on any machine. 

## Also check out FileMover over at https://filemover-b263cc.gitlab.io (src at https://gitlab.com/dant41/filemover)
A simple in-browser way to send files peer-to-peer, with no fuss! 

## TO VIEW SOURCE CODE FOR DW2 DEMO, SEE https://gitlab.com/dant41/dw2/

### This repository should be kept private whenever it is not in use. All commits and administrativa should be done from my main account. 

# Changelog

#### The following will track what files should be within this repository, and what builds/dates are associated with them
### 4/20/24  Rasdoon Demo v1 (Godot 3)
### 4/20/24  Lucky There's A Family Guy (Lethal Company Mod)
### 4/30/24  Next Order Mod
### 6/6/24  DW2 Demo v0.1 (Windows only, simply run the exe)
### 8/27/2024 DW2 Demo Experimental Mac0s build. See https://docs.godotengine.org/en/stable/tutorials/export/running_on_macos.html#app-is-signed-including-ad-hoc-signatures-but-not-notarized to find out how to run, since this is unsigned you will need to grant permissions
### 2/22/2025 linked to FileMover